#!/usr/bin/env python
"""Tests for the zpt module.

Authors
-------
    Johannes Sahlmann

"""

import os

import numpy as np
import pandas as pd

from .. import zpt

LOCAL_DIR = os.path.dirname(os.path.abspath(__file__))

def test_zpt():
    """Test zpt functions."""
    zpt.load_tables()
    print('')
    """Test zpt functions."""
    df = pd.read_csv(os.path.join(LOCAL_DIR, 'tableZ5Z6.txt'))
    df['ecl_lat'] = np.rad2deg(np.arcsin(df['sinBeta']))
    df['phot_g_mean_mag'] = df['G']
    df['nu_eff_used_in_astrometry'] = df['nuEff']
    df['pseudocolour'] = df['nuEff']
    df['astrometric_params_solved'] = 31
    # df['zpt'] = df.apply(zpt.zpt_wrapper, axis=1)
    df['zpt5'] = zpt.get_zpt(df['phot_g_mean_mag'], df['nu_eff_used_in_astrometry'], df['pseudocolour'], df['ecl_lat'], df['astrometric_params_solved'] )
    pd.testing.assert_series_equal(df['zpt5'], df['Z5'], check_names=False)
    # pd.testing.assert_series_equal(df.apply(zpt.zpt_wrapper, axis=1), df['Z5'], check_names=False)

    df['astrometric_params_solved'] = 95
    df['zpt6'] = zpt.get_zpt(df['phot_g_mean_mag'], df['nu_eff_used_in_astrometry'], df['pseudocolour'], df['ecl_lat'], df['astrometric_params_solved'] )
    pd.testing.assert_series_equal(df['zpt6'], df['Z6'], check_names=False)
    